
var amqp = require('amqplib');
var inherits = require('util').inherits;
var EventEmitter = require('events');

var connections = {};

exports.connect = function(connectionString) {

    var connectionEvent = connections[connectionString];
    if (!connectionEvent) {
        connectionEvent = new EventEmitter();
        connections[connectionString] = connectionEvent;
    }

    if (connectionEvent.isConnecting) {
        return connectionEvent;
    }

    if (connectionEvent.connection) {
        process.nextTick(function() {
            connections[connectionString].emit('connected', connections[connectionString].connection);
        });
        return connectionEvent;
    }

    connectionEvent.isConnecting = true;
    amqp.connect(connectionString)
        .then(function(connection) {
            connectionEvent.isConnecting = false;
            connectionEvent.connection = connection;

            connection.on('error', function(err) {
                console.error(err);
            });

            connection.on('close', function() {
                console.log('close');
                delete connectionEvent.connection;
                connectionEvent.emit('disconnected');
                exports.reconnect(connectionString);
            });

            connectionEvent.emit('connected', connectionEvent.connection);
        })
        .catch(function(err) {
            console.error(err);
            delete connectionEvent.connection;
            connectionEvent.isConnecting = false;
            exports.reconnect(connectionString);
        });

    return connectionEvent;
};

exports.reconnect = function(connectionString) {
    setTimeout(function() {
        exports.connect(connectionString);
    }, 1000);
};