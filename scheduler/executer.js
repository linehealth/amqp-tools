
var AMQPChannel = require('../channel');
var EventEmitter = require('events');
var inherits = require('util').inherits;

function Executer(callback, options) {
    EventEmitter.call(this);

    if (!options) {
        throw new Error('No options');
    }
    if (!options.amqp) {
        throw new Error('No connection string for AMQP server');
    }
    if (!options.exchangeName) {
        throw new Error('No exchange name');
    }

    this.options = options;
    this.callback = callback;
    this.channel = new AMQPChannel(options.amqp);

    var _this = this;
    this.channel.on('connected', function() {

        this.assertExchange(options.exchangeName, 'x-delayed-message', {
            durable: true,
            arguments: {
                'x-delayed-type': 'fanout'
            }
        }).then(function() {
            _this.channel.assertQueue(options.exchangeName, {durable: true})
                .then(function() {
                    _this.channel.bindQueue(options.exchangeName, _this.options.exchangeName);
                    _this.channel.consume(options.exchangeName, function(message) {
                        _this.callback(JSON.parse(message.content.toString()));
                    }, { noAck: true });
                    _this.emit('connected');
                }).catch(function(err) {
                    console.log(err);
                });
        });

    });

    this.channel.on('disconnected', function() {
        _this.emit('disconnected');
    });
}

inherits(Executer, EventEmitter);

module.exports = Executer;
