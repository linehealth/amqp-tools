
var AMQPChannel = require('../channel');
var EventEmitter = require('events');
var inherits = require('util').inherits;

function Scheduler(options) {
    EventEmitter.call(this);

    if (!options) {
        throw new Error('No options');
    }
    if (!options.amqp) {
        throw new Error('No connection string for AMQP server');
    }
    if (!options.exchangeName) {
        throw new Error('No namespace');
    }

    this.options = options;
    this.scheduleChannel = new AMQPChannel(options.amqp);

    var _this = this;
    this.scheduleChannel.on('connected', function() {
        this.assertExchange(options.exchangeName, 'x-delayed-message', {
            durable: true,
            arguments: {
                'x-delayed-type': 'fanout'
            }
        }).then(function() {
            _this.emit('connected');
        });
    });

    this.scheduleChannel.on('disconnected', function() {
        _this.emit('disconnected');
    });
}

inherits(Scheduler, EventEmitter);

Scheduler.prototype.schedule = function(fireDate, data) {
    var fireDateTime = fireDate.getTime();
    var nowTime = new Date();
    nowTime = nowTime.getTime();

    var delay = 0;
    if (fireDateTime > nowTime) {
        delay = fireDateTime - nowTime;
    }

    var buffer = new Buffer(JSON.stringify(data));
    var options = {
        headers: {
            'x-delay': delay
        }
    };

    return this.scheduleChannel.publish(this.options.exchangeName, this.options.exchangeName, buffer, options);
};

module.exports = Scheduler;
