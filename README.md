# AMQP Tools

## RPC

#### Server example
```
var AMQP = require('amqp-tools');

var rpcServer = new AMQP.RPCServer({
    amqp: 'amqp://localhost',
    queueName: 'myrpc'
});

rpcServer.method('getThing', function(thingId, callback) {

    // Database query

    callback({
        thing_id: 1,
        name: 'Thing name'
    });

});

```

#### Client example
```
var AMQP = require('amqp-tools');

var rpcClient = new AMQP.RPCClient({
    amqp: 'amqp://localhost',
    queueName: 'myrpc',
    timeout: 30000 // milliseconds
});

rpcClient.once('connected', function() {

    var thingId = 1;

    rpcClient.call('getThing', thingId, function(err, thing) {
        if (err) {
            console.log(err);
            return;
        }
        console.log('Thing:', thing);
    });

});
```


## PUBLISHER / SUBSCRIBER

#### Publisher example

```
var AMQP = require('amqp-tools');

var publisher = new AMQP.Publisher({
    amqp: 'amqp://localhost',
    exchangeName: 'mypubsub'
});

publisher.publish('mytopic', 'my message');
```

#### Subscriber example

```
var AMQP = require('amqp-tools');

var subscriber = new AMQP.Subscriber('#', function(topic, message) {

    console.log(topic, message);

}, {
    amqp: 'amqp://localhost',
    exchangeName: 'mypubsub'
});
```


## SCHEDULER

#### Scheduler example

```
var AMQP = require('amqp-tools');

var scheduler = new AMQP.Scheduler({
    amqp: 'amqp://localhost',
    exchangeName: 'myscheduler'
});

scheduler.once('connected', function() {
    var fireDate = new Date();
    fireDate = parseInt(fireDate.getTime() / 1000) + 30;
    fireDate = new Date(fireDate * 1000);

    scheduler.schedule(fireDate, {my_attribute: 1});
});
```

#### Executer example

```
var AMQP = require('amqp-tools');

var executer = new AMQP.Executer(function(message) {
    console.log("execute", message);
}, {
    amqp: 'amqp://localhost',
    exchangeName: 'myscheduler'
});
```
