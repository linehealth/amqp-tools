
var EventEmitter = require('events');
var Promise = require('bluebird');
var uuid = require('uuid');
var inherits = require('util').inherits;
var AMQPChannel = require('../channel');

function Subscriber(key, callback, options) {
    EventEmitter.call(this);

    if (!options) {
        throw new Error('No options');
    }
    if (!options.amqp) {
        throw new Error('No connection string for AMQP server');
    }
    if (!options.exchangeName) {
        throw new Error('No exchange name');
    }

    this.key = key;
    this.callback = callback;
    this.options = options;
    this.channel = new AMQPChannel(options.amqp);

    var _this = this;
    this.channel.on('connected', function() {
        _this.start();
    });
}

inherits(Subscriber, EventEmitter);

Subscriber.prototype.start = function() {
    var _this = this;

    this.channel.assertExchange(this.options.exchangeName, 'topic', {durable: true})
        .then(function() {
          var options = {};
          var queueName = '';
          if (_this.options.queue) {
            queueName = _this.options.queue;
            options.durable = true;
          } else {
            options.exclusive = true;
          }
          _this.channel.assertQueue(queueName, options)
              .then(function(q) {
                  _this.channel.bindQueue(q.queue, _this.options.exchangeName, _this.key);
                  _this.channel.consume(q.queue, function(message) {
                      _this.callback(message.fields.routingKey, JSON.parse(message.content.toString()));
                  }, { noAck: true });
              }).catch(function(err) {
                  console.log(err);
              });
        })
        .catch(function(err) {
            console.log(err);
        });
};

module.exports = Subscriber;
