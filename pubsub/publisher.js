
var EventEmitter = require('events');
var Promise = require('bluebird');
var uuid = require('uuid');
var inherits = require('util').inherits;
var AMQPChannel = require('../channel');

function Publisher(options) {
    EventEmitter.call(this);

    if (!options) {
        throw new Error('No options');
    }
    if (!options.amqp) {
        throw new Error('No connection string for AMQP server');
    }
    if (!options.exchangeName) {
        throw new Error('No exchange name');
    }

    this.options = options;
    this.buffer = [];
    this.channel = new AMQPChannel(options.amqp);

    var _this = this;
    this.channel.on('connected', function() {
        _this.channel.assertExchange(options.exchangeName, 'topic', {durable: true});
    });
}

inherits(Publisher, EventEmitter);

Publisher.prototype.flushBuffer = function() {
    var _this = this;

    var promises = [];

    this.buffer.forEach(function(message) {
        if (message.isSending) {
            return;
        }
        var buffer = new Buffer(JSON.stringify(message.data));
        message.isSending = true;
        var promise = _this.channel.publish(_this.options.exchangeName, message.key, buffer)
            .then(function() {
                message.sent = true;
            }).finally(function() {
                message.isSending = false;
            });
        promises.push(promise);
    });

    Promise.all(promises)
        .finally(function() {
            _this.cleanSentMessages();
        });
};

Publisher.prototype.cleanSentMessages = function() {
    this.buffer = this.buffer.filter(function(message) {
        return !message.sent;
    });
};

Publisher.prototype.publish = function(key, data) {
    this.buffer.push({
        key: key,
        data: data,
        sent: false
    });
    this.flushBuffer();
};

module.exports = Publisher;
