
var AMQPChannel = require('../channel');
var Promise = require('bluebird');
var uuid = require('uuid');
var EventEmitter = require('events');
var inherits = require('util').inherits;

function RPCClient(options) {
    EventEmitter.call(this);

    if (!options) {
        throw new Error('No options');
    }
    if (!options.amqp) {
        throw new Error('No connection string for AMQP server');
    }
    if (!options.queueName) {
        throw new Error('No queue name');
    }
    if (!options.timeout) {
        options.timeout = 30000;
    }

    this.options = options;
    this.responseChannel = new AMQPChannel(options.amqp);
    this.requestChannel = new AMQPChannel(options.amqp);
    this.deadLetterChannel = new AMQPChannel(options.amqp);
    this.requests = [];

    var _this = this;
    this.responseChannel.on('connected', function() {
        this.assertQueue('', {exclusive: true})
            .then(function(q) {
                _this.responseQueue = q.queue;
                _this.consumeResponses();
                _this.emit('connected');
            })
            .catch(function(err) {
                delete _this.responseQueue;
                console.error(err);
            });
    });

    this.responseChannel.on('disconnected', function() {
        delete _this.responseQueue;
        _this.emit('disconnected');
    });

    this.requestChannel.on('connected', function() {
        var exchangeName = _this.options.queueName + '.deadletter';

        var channel = this;
        channel.assertExchange(exchangeName, 'fanout', {durable: false})
            .then(function() {
                channel.assertQueue(_this.options.queueName, {
                    durable: false,
                    arguments: {
                        "x-dead-letter-exchange": exchangeName,
                        "x-message-ttl": _this.options.timeout
                    }
                });
            });
    });

    this.deadLetterChannel.on('connected', function() {
        var exchangeName = _this.options.queueName + '.deadletter';

        var channel = this;
        channel.assertQueue('', {exclusive: true})
            .then(function(q) {
                channel.bindQueue(q.queue, exchangeName);
                channel.consume(q.queue, function(message) {
                    var correlationId = message.properties.correlationId;
                    _this.requests = _this.requests.filter(function(request) {
                        if (request.correlationId === correlationId) {
                            if (request.callback) {
                                process.nextTick(function() {
                                    request.callback(new Error('timeout'));
                                });
                            }
                            return false;
                        }
                        return true;
                    });
                }, { noAck: true });
            }).catch(function(err) {
                console.log(err);
            });
    });

}

inherits(RPCClient, EventEmitter);

RPCClient.prototype.consumeResponses = function() {
    var _this = this;
    this.responseChannel.consume(_this.responseQueue, function(message) {
        var correlationId = message.properties.correlationId;
        var responseData = JSON.parse(message.content.toString());

        _this.requests = _this.requests.filter(function(request) {
            if (request.correlationId === correlationId) {
                if (responseData.s !== 'ok') {
                    process.nextTick(function() {
                        request.callback(new Error(responseData.s));
                    });
                } else {
                    process.nextTick(function() {
                        console.log('call callback');
                        request.callback.call(null, responseData.r);
                    });
                }
                return false;
            }
            return true;
        });

    }, { noAck: true });
};

RPCClient.prototype.call = function(method) {
    var args = Array.prototype.slice.call(arguments, 1);

    var callback;
    if (args.length > 0 && typeof args[args.length - 1] === 'function') {
        callback = args[args.length - 1];
        args.pop();
    }

    if (!this.responseQueue) {
        if (callback) {
            callback(new Error('No connection'));
        }
        return;
    }

    var request = {};

    var data = {
        m: method,
        a: args
    };

    var data = new Buffer(JSON.stringify(data));

    var parameters = {
        contentType: 'application/json'
    };

    if (callback) {
        request.correlationId = uuid.v4();
        request.callback = callback;

        parameters.replyTo = this.responseQueue;
        parameters.correlationId = request.correlationId;
    }

    this.requestChannel.sendToQueue(this.options.queueName, data, parameters);

    if (callback) {
        this.requests.push(request);
    }
};

module.exports = RPCClient;
