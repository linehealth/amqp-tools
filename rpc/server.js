
var AMQPChannel = require('../channel');

function RPCServer(options) {

    if (!options) {
        throw new Error('No options');
    }
    if (!options.amqp) {
        throw new Error('No connection string for AMQP server');
    }
    if (!options.queueName) {
        throw new Error('No queue name');
    }

    this.options = options;
    this.responseChannel = new AMQPChannel(options.amqp);
    this.requestChannel = new AMQPChannel(options.amqp);
    this.methods = {};

    var _this = this;
    this.requestChannel.on('connected', function() {
        var queueName = _this.options.queueName;
        this.assertQueue(queueName, {durable: false});
        this.consume(queueName, function(message) {
            var request = JSON.parse(message.content.toString());
            var properties = message.properties;

            if (_this.methods[request.m]) {
                try {
                    var requestArguments = request.a;
                    requestArguments[requestArguments.length] = function(result) {
                        if (properties.replyTo && properties.correlationId) {
                            var data = new Buffer(JSON.stringify({
                                s: 'ok',
                                r: result
                            }));
                            _this.responseChannel.sendToQueue(properties.replyTo, data, {correlationId: properties.correlationId});
                        }
                    };
                    _this.methods[request.m].apply(_this, requestArguments);
                } catch(ex) {
                    if (properties.replyTo && properties.correlationId) {
                        var data = new Buffer(JSON.stringify({
                            s: 'exception'
                        }));
                        _this.responseChannel.sendToQueue(properties.replyTo, data, {correlationId: properties.correlationId});
                    }
                }
            } else if (properties.replyTo && properties.correlationId) {
                var data = new Buffer(JSON.stringify({
                    s: 'not_found'
                }));
                _this.responseChannel.sendToQueue(properties.replyTo, data, {correlationId: properties.correlationId});
            }

        }, {noAck: true});
    });
}

RPCServer.prototype.method = function(method, callback) {
    this.methods[method] = callback;
};

module.exports = RPCServer;