
var amqp = require('amqplib');
var EventEmitter = require('events');
var inherits = require('util').inherits;
var AMQPConnectionManager = require('./connection');
var Promise = require('bluebird');

function Channel(connectionString) {
    EventEmitter.call(this);
    this.connectionListenner = AMQPConnectionManager.connect(connectionString);

    var _this = this;
    this.connectionListenner.on('connected', function(connection) {
        delete _this.channel;
        _this.connection = connection;
        _this.createChannel();
    });

    this.connectionListenner.on('disconnected', function() {
        delete _this.connection;
        delete _this.channel;
        _this.emit('disconnected');
    });
}

inherits(Channel, EventEmitter);

Channel.prototype.createChannel = function() {
    var _this = this;
    return new Promise(function(resolve, reject) {
        if (_this.channel) {
            return resolve(_this.channel);
        }

        if (!_this.connection) {
            return reject(new Error('No connection'));
        }

        _this.channel = _this.connection.createChannel()
            .then(function(channel) {

                channel.on('close', function() {
                    delete _this.channel;
                });

                _this.channel = channel;
                _this.emit('connected');
                resolve(channel);
            })
            .catch(function(err) {
                reject(err);
            });
    });
};

Channel.prototype.assertExchange = function() {
    var args = arguments;
    return this.createChannel()
        .then(function(channel) {
            return channel.assertExchange.apply(channel, args);
        });
};

Channel.prototype.publish = function() {
    var args = arguments;
    return this.createChannel()
        .then(function(channel) {
            return channel.publish.apply(channel, args);
        });
};

Channel.prototype.sendToQueue = function() {
    var args = arguments;
    return this.createChannel()
        .then(function(channel) {
            return channel.sendToQueue.apply(channel, args);
        });
};

Channel.prototype.bindQueue = function() {
    var args = arguments;
    return this.createChannel()
        .then(function(channel) {
            return channel.bindQueue.apply(channel, args);
        });
};

Channel.prototype.consume = function() {
    var args = arguments;
    return this.createChannel()
        .then(function(channel) {
            return channel.consume.apply(channel, args);
        });
};

Channel.prototype.assertQueue = function() {
    var args = arguments;
    return this.createChannel()
        .then(function(channel) {
            return channel.assertQueue.apply(channel, args);
        });
};

module.exports = Channel;
